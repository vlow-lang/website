module.exports = (md) => {
    md.use(require('markdown-it-anchor'), {permalink: true})
    md.use(require('markdown-it-footnote'))
}