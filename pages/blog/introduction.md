---
title: introduction
shadow: 前書き
date: 2020-04-10
---

Back in the day most people could not write. If you needed something written, you would hire a scribe, and they would write it down. Scribes were people trained in special institutions in writing.

Eventually more people were able to write, and writing started being used as the extension of one's mind. It is hard to multiply eight-digit number in mind, but it's easy to do on paper. Writing allows us to think thoughts we could not think before.

Nowadays most people can not program. If you need something programmed, you hire a developer, and they would program it. Developers are people trained in special institutions in programming.

Eventually more people will be able to program, and programing will be used an extension of one's mind. There is plethora of tasks hard to do in mind or on paper, but it's easy to let a computer do it. Programing allows us to think thoughts we could not think before.

**`vlow`'s goal is to aid in that**

## Programing in `vlow` should be as easy as sketching on paper

We aim to develop a universally applicable language that is easy to start working with and easy to reason about.

- `vlow` is to be a universally applicable
  
  by compiling `vlow` to native executables there are no restriction where `vlow` code can be used: be it a webservice, or a simple gui tool

- graphical language
  
  as those have a reputation of being easy to get started for non-technical people, by eliminating the burden of syntax and typos
  
- using the dataflow oriented paradigm
  
  as this is a pattern that is natural to think and reason about
  
- having a strong static type-system, with powerful generic and templating system
  
  as this will prevent a lot of mistakes and allows for great autocompletion and development time code checks

- and strong tooling support

  an IDE, a debugger, version control support, testing frameworks are essential for a language to ever hope to hold its own

## Development

Currently `vlow` is in no shape to be presented to the world, but we strive to post bi-weekly updates on this blog. 

```vlow
[
  {"id":"BD346D10-4A7B-422F-889B-13100D043AB2","type":"entry","x":-311,"y":12,"node":{}},
  {"id":"0CBDD21B-03EA-4488-98F7-11FD94C374A5","type":"mapToValue","x":-135,"y":12,"node":{
    "$0":{"type":"connection","connection":{"source":"BD346D10-4A7B-422F-889B-13100D043AB2","pin":"out"}},
    "$1":{"type":"constant","constant":{"value":"\"stay tuned\""}}}},
  {"id":"424390E7-EE8D-495F-9116-9BA8999C910B","type":"print","x":161,"y":12,"node":{
    "string":{"type":"connection","connection":{"source":"0CBDD21B-03EA-4488-98F7-11FD94C374A5","pin":"out"}}}}
]
```
