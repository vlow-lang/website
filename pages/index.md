---
title: vlow - visual reactive dataflow programming language
index: ./blog/
---

::: slot demo
@[code](./demo.vlow)
:::

::: slot body
vlow is an **upcoming** general purpose visual programming language embracing rx streams as first class language concept; vlow has templates/ generics, protocols/ interfaces and loads of other fancy stuff
:::
