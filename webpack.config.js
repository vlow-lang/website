const path = require('path');
const VueLoader = require("vue-loader");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const MinipressWebpackPlugin = require('@vlow-lang/minipress')


module.exports = (env, argv) => {
    return  {
        devtool: "source-map",
        entry: "./index.js",
        mode: env.WEBPACK_SERVE ?  'development' : 'production',
        output: {
            filename: "bundle.js",
            clean: true,
            library: 'website',
            libraryTarget: 'commonjs2',
            publicPath: '/',
        },
        module: {
            rules: [
                {
                    test: /\.vue$/,
                    loader: 'vue-loader',
                    options: {
                        loaders: {
                            scss: 'style-loader!css-loader!sass-loader'
                        },
                    },
                },
                {
                    test: /\.(png|jpe?g|gif|svg|woff2?)$/i,
                    type: 'asset/resource',
                    generator: {
                        filename: 'static/[name][ext]'
                    }
                },
                {
                    test: /\.css$/i,
                    use: [
                        env.WEBPACK_SERVE ? 'style-loader' : MiniCssExtractPlugin.loader,
                        'css-loader',
                    ],
                },
                {
                    test: /\.scss$/,
                    use: [
                        env.WEBPACK_SERVE ? 'style-loader' : MiniCssExtractPlugin.loader,
                        'css-loader',
                        'sass-loader'
                    ]
                },
                {
                    test: /\.md$/,
                    use: [
                        { loader: "vue-loader"},
                        { loader: MinipressWebpackPlugin.markdownLoader }
                    ]
                },
                {
                    test: /site\.config$/,
                    use: [
                        { loader: MinipressWebpackPlugin.routesLoader }
                    ]
                },
            ]
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: 'static/main.css'
            }),
            new VueLoader.VueLoaderPlugin(),
            env.WEBPACK_SERVE ? new HtmlWebpackPlugin({
                // hash: true,
                title: 'My Awesome application',
                myPageHeader: 'Hello World',
                template: './index.development.html',
                filename: './index.html'
            }) : new MinipressWebpackPlugin()
        ],
        devServer: {
            // contentBase: path.join(__dirname, 'dist'),
            // compress: true,
            port: 4647,
            historyApiFallback: {
                rewrites: [
                    { from: /\.*/, to: `/` }
                ]
            },
            disableHostCheck: true
        },
        resolve: {
            alias: {
                "@": path.resolve(__dirname),
                // "~": path.resolve(__dirname)
            },
        }
    }
}
